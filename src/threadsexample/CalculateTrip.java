package threadsexample;

public class CalculateTrip implements Runnable {

    private String name;
    private int time;


    public CalculateTrip(String name, int time) {
        this.name = name;
        this.time = time;
    }

    public void run() {
        System.out.println("\n" + name + " is starting their trip, it will take "
                + time + " hours\n");
        for (int count = 1; count < time; count++) {
                System.out.print(name + " is traveling. \n");
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
        }
        System.out.println("\n" + name + " has arrived.\n");
    }
}