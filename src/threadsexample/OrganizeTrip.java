package threadsexample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Scanner;
import java.util.ArrayList;


class Traveler {
    String name;
    int time;

    Traveler(String Name, int Time){
        this.name= Name;
        this.time= Time;
    }

}

public class OrganizeTrip {

    public static void main(String[] args) {
        ArrayList<Traveler> Travelers = new ArrayList<>();
        boolean addUser = true;

        System.out.println("This program will report traveler's status.\n" +
                "Whether they have begun, in the middle of, or finished with their journey.\n");


        do {
            System.out.println("Please enter the travelers name:");
            Scanner myObj = new Scanner(System.in);
            String userName = myObj.nextLine();
            int userTime = 0;

            boolean valid = false;
            while(!valid) {
                try {
                    System.out.println("Enter the number of hours it will take for their quest.");
                    userTime = myObj.nextInt();
                    valid = true;
                } catch (Exception InputMismatchException){
                    System.out.println("that's not a valid number silly");
                    valid = false;
                    myObj.nextLine();
                }
            }

            System.out.println("Name: " + userName + " Time: " + userTime);
            Travelers.add(new Traveler(userName,userTime));
            boolean done = false;
            while(!done)
            {
                Scanner input = new Scanner(System.in);
                System.out.println("Would you like to enter another person? Y/N");
                String ans = input.nextLine();
                if (ans.equals("Y")) {
                    addUser = true;
                    done= true;
                } else if (ans.equals("N")) {
                    addUser = false;
                    done = true;
                } else {
                    System.out.println("Please enter Y or N");
                }
            }
        }while(addUser);

        ExecutorService myService = Executors.newFixedThreadPool(3);

      for (int i=0; i<Travelers.size();i++){
            CalculateTrip trip = new CalculateTrip(Travelers.get(i).name, Travelers.get(i).time);
            myService.execute(trip);
        }
      myService.shutdown();
    }
}
