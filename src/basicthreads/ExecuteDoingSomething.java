package basicthreads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDoingSomething {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        DoingSomething ds1 = new DoingSomething("Bob", 10, 30);
        DoingSomething ds2 = new DoingSomething("Sally", 7, 25);
        DoingSomething ds3 = new DoingSomething("Billy", 4, 20);
        DoingSomething ds4 = new DoingSomething("Anna", 2, 15);
        DoingSomething ds5 = new DoingSomething("Pat", 1, 10);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
