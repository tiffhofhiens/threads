package concurrency;

public class CreatingStringThreads {
//
//    public static class MyThread extends Thread {
//        @Override
//        public void run(){
//            System.out.println("Hello World");
//        }
//    }

    public static class MyRunnable implements Runnable{
        @Override
        public void run(){
            System.out.println("Hello World Runnable");
        }
    }

    public static void main(String[] args){
//        Thread myThread =  new MyThread();
        Thread myThread =  new Thread(new MyRunnable());
        myThread.start();
    }
}
